import { configureStore } from "@reduxjs/toolkit";
import productCart from "./productCart";
export const store = configureStore({
  reducer: {
    product: productCart,
  },
});
