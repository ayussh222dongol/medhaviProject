import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface ProductState {
  products: any[];
}

const initialState: ProductState = {
  products: [],
};

const productCart = createSlice({
  name: "product",
  initialState,
  reducers: {
    addProduct: (state, action: PayloadAction<any>) => {
      state.products.push(action.payload);
    },
    removeProduct: (state, action: PayloadAction<number>) => {
      state.products.splice(action.payload, 1);
    },
  },
});

export const { addProduct, removeProduct } = productCart.actions;
export default productCart.reducer;
