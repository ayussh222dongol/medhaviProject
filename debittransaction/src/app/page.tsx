'use client'
import Image from 'next/image'
import DebitNote from "./pages/debit-notepage/page"
import { Provider } from 'react-redux'
import { store } from './store/store'
export default function Home() {
  return (
    <Provider store={store}>
      <DebitNote />
    </Provider>
  )
}
