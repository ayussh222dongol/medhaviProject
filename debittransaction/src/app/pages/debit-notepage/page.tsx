
import React from 'react'
import Close from "../../../../public/images/Cross.svg"
import Image from 'next/image'
import SupplierData from '@/app/components/SupplierData'
import ProductTable from '@/app/components/ProductTable'
import { IoIosArrowForward } from "react-icons/io"
import RightArrow from "../../../../public/images/RightArrow.svg"
const page = () => {
  return (
    <div className='flex flex-col items-center justify-center mt-5 '>
      <div className='w-[90%] flex flex-col items-center justify-center bg-[white] rounded-lg p-[24px] '>
        <div className='flex items-center justify-between w-full'>
          <p className='text-[17px] font-[700] leading-[22px]'>New debit note</p>
          <Image src={Close} alt="close" className='h-[24px] w-[24px]' />
        </div>
        <div className='w-full pt-[37px] flex flex-col gap-[16px]'>
          <SupplierData />
          <ProductTable />
          <div className="flex items-center justify-between border-b border-[#F0F0F0]">
            <p className="text-[18px] leading-[20px] py-[28px] border-b border-[#F0F0F0]">Custom fields</p>
            <Image src={RightArrow} alt="arrow" className="h-5 w-5" />
          </div>
          <div>
            <p className="text-[14px] font-[400] leading-[16px] mb-[8px]">Terms & conditions</p>
            <textarea className="px-[16px] py-[15px] bg-[#F0F0F0] w-full rounded-[8px]" placeholder='Enter notes' />
          </div>
          <div className="flex items-center justify-end">
            <button className=" px-[40px] py-[14px] text-[white] rounded-[8px] bg-[#00A8E8] ">Save</button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default page