'use client'
import React, { Fragment } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { searchData } from "@/app/assets/customData/SearchData"
import { AiOutlineSearch, AiOutlineClose } from "react-icons/ai"
import { useDispatch, useSelector } from 'react-redux'
import { addProduct, removeProduct } from '../store/productCart'
import { IoIosArrowDown } from "react-icons/io"
const TableHeader = [
  {
    id: 1,
    title: "Item/product"
  },
  {
    id: 2,
    title: "Batch"
  },
  {
    id: 3,
    title: "Warehouse"
  },
  {
    id: 4,
    title: "Qty"
  },
  {
    id: 5,
    title: "Rate"
  },
  {
    id: 6,
    title: "Discount"
  },
  {
    id: 7,
    title: "Tax"
  },
  {
    id: 8,
    title: "Amount"
  },
]

const totalAmountData = [
  {
    id: 1,
    title: "Total",
    amount: 0
  },
  {
    id: 2,
    title: "Total Excise Duty",
    amount: 0
  },
  {
    id: 3,
    title: "Discount Amount",
    amount: 0
  },
  {
    id: 4,
    title: "Non-taxable Total",
    amount: 0
  },
  {
    id: 5,
    title: "Taxable Total",
    amount: 0
  },
  {
    id: 6,
    title: "VAT",
    amount: 0
  },
]

const ProductTable = () => {
  const dispatch = useDispatch();
  const productCart = useSelector((state: any) => state.product.products);
  console.log(productCart, "productCart")
  const data: any = []
  const [isOpen, setIsOpen] = React.useState(false)
  const [searchQuery, setSearchQuery] = React.useState('');
  const filteredData = searchData.filter(item =>
    item.title.toLowerCase().includes(searchQuery.toLowerCase())
  );
  const handleAddToCart = (details: any) => {
    dispatch(addProduct(details));
    setIsOpen(false)
  };
  return (
    <div className='border-b border-[#F0F0F0] pb-[24px]'>
      <table className="w-full overflow-x-scroll">
        <thead>
          <tr className='border-y border-[#F0F0F0] '>
            {TableHeader?.map((details: any) =>
              <th key={details?.id} className='text-start py-[15px] text-[15px] font-[600] leading-[18px]'>{details?.title}</th>
            )}
            <th></th>
          </tr>
        </thead>
        <tbody className='text-[12px] leading-[16px] font-[500]'>
          {productCart?.length ?
            productCart?.map((details: any, index: number) =>
              <tr key={details?.id} className=''>
                <td className='pb-5 pt-2  '>
                  <div className='flex flex-col gap-[5px]'>
                    <p>{details?.title}</p>
                    <input placeholder="Enter Description" className='focus:outline-none p-[8px] text-[11px] border-b-2 border-gray border-dashed leading-[13px] w-[40%]' />
                  </div>
                </td>
                <td className='pb-5 '>{details?.batch}</td>
                <td className='pb-5 '>
                  <select name="cars" id="">
                    <option value="volvo">Ktm1</option>
                    <option value="saab">Ktm2</option>
                    <option value="opel">Ktm3</option>
                    <option value="audi">Ktm4</option>
                  </select>
                </td>
                <td className=''>
                  <div className=' border-b-2 border-dashed pb-5  flex items-center justify-between gap-[8px] mr-1 w-[60%]'>
                    <p>2</p>
                    <select name="cars" id="" className="text-[#979797] text-[12px] leading-[16px] select-no-border" >
                      <option value="volvo">Btl1</option>
                      <option value="saab">Btl2</option>
                      <option value="opel">Btl3</option>
                      <option value="audi">Btl4</option>
                    </select>


                  </div>
                </td>
                <td className=''>
                  <div className='pb-5  border-b-2 border-dashed  mr-1'>2300</div>
                </td>
                <td className=''>
                  <div className='pb-5 border-b-2 border-dashed w-[80%]'>230</div>
                </td>
                <td className='pb-5 '>13% VAT</td>
                <td className='pb-5 '>{details?.price}</td>
                <td className='pb-5 '><AiOutlineClose className="text-[red] h-5 w-5" onClick={() => dispatch(removeProduct(index))} /></td>
              </tr>
            )
            :
            <p className='text-[15px] font-[400] text-[#979797] leading-[16px] py-[20px] cursor-pointer'
              onClick={() => setIsOpen(true)}>Add code or product</p>
          }
          {productCart?.length > 0 ? <p className='text-[15px] font-[400] text-[#979797] leading-[16px] py-[20px] cursor-pointer'
            onClick={() => setIsOpen(true)}>Add code or product</p> : null}
        </tbody>
      </table>
      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={() => setIsOpen(false)}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto">
            <div className="flex min-h-full items-center justify-center p-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="w-[500px] h-[408px] transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all relative">
                  <Dialog.Title
                    as="h3"
                    className="text-lg font-medium leading-6 text-gray-900"
                  >
                    <div className='flex items-center w-full gap-[8px] p-[8px] rounded-[8px] bg-[#F0F0F0]'>
                      <AiOutlineSearch />
                      <input
                        className='flex-1 focus:outline-none bg-[#F0F0F0] text-[14px] leading-[16px]'
                        placeholder='Search product'
                        value={searchQuery}
                        onChange={(e) => setSearchQuery(e.target.value)}
                      />
                    </div>
                  </Dialog.Title>
                  <>
                    <div className='flex flex-col gap-[8px] mt-[12px] w-full h-[90%] overflow-y-scroll px-[16px]'>
                      {filteredData?.length > 0 ?
                        filteredData?.map((details: any) =>
                          <div
                            key={details?.id}
                            className='flex w-full items-center justify-between pb-[8px] border-b border-[#F0F0F0] cursor-pointer'
                            onClick={() => handleAddToCart(details)}
                          >
                            <div className='flex flex-col gap-[4px]'>
                              <p className='text-[14px] font-[600] leading-[18px]'>{details?.title}</p>
                              <span className='flex items-center text-[11px] text-[#00A8E8] gap-[16px]'>{details?.sub_id}<p className='text-[#979797]'>{details?.batch}</p></span>
                            </div>
                            <p>Rs. {details?.price}</p>
                          </div>) : null}
                    </div>
                    <div className='flex items-center justify-between px-20 w-full py-[12px] absolute bottom-0 left-0 right-0 bg-white'>
                      <p className='text-[12px] text-[#00A8E8] font-[600] leading-[16px] cursor-pointer'>Add New</p>
                      <p className='text-[12px] text-[#00A8E8] font-[600] leading-[16px] cursor-pointer' onClick={() => setIsOpen(false)}>Close</p>
                    </div>
                  </>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
      <div className='flex  justify-between w-full gap-[64px]'>
        <div className="w-full flex flex-col gap-[8px]">
          <p className='text-[12px] font-[500] leading-[16px]'>Note</p>
          <textarea placeholder='Enter notes' className='rounded-[8px] bg-[#F0F0F0] p-[15px] text-[12px] font-[500] leading-[16px] ' />
          <p className='text-[12px] font-[500] leading-[16px] text-[#979797]'>*This will appear on print</p>
        </div>
        <div className="w-full p-[8px]">
          <div className="flex flex-col gap-[12px] border-b-[2px] border-black">
            {totalAmountData?.map((details: any) =>
              <div className='flex items-center justify-between w-full pb-2' key={details?.id}>
                <p className='text-[12px] font-[400] leading-[16px]'>{details?.title}</p>
                <p className='text-[12px] font-[400] leading-[16px]'>{details?.amount}</p>
              </div>
            )}
          </div>
          <div className='flex items-center justify-between py-2'>
            <p className='text-[12px] font-[400] leading-[16px]'>Grand Total</p>
            <p className='text-[12px] font-[400] leading-[16px]'>0</p>
          </div>
        </div>
      </div>
    </div >
  )
}

export default ProductTable