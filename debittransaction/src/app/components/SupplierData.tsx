'use client'
import { Combobox, Transition } from '@headlessui/react'
import React, { Fragment } from 'react'
import { IoIosArrowDown } from "react-icons/io"
import { useSelector } from 'react-redux'
const people = [
  { id: 1, name: 'Wade Cooper' },
  { id: 2, name: 'Arlene Mccoy' },
  { id: 3, name: 'Devon Webb' },
  { id: 4, name: 'Tom Cook' },
  { id: 5, name: 'Tanya Fox' },
  { id: 6, name: 'Hellen Schmidt' },
]
const SupplierData = () => {
  const [selected, setSelected] = React.useState(people[0])
  const [query, setQuery] = React.useState('')
  const filteredPeople =
    query === ''
      ? people
      : people.filter((person) =>
        person.name
          .toLowerCase()
          .replace(/\s+/g, '')
          .includes(query.toLowerCase().replace(/\s+/g, ''))
      )
  return (
    <div className='flex flex-col gap-[16px]'>
      <div className='flex items-center justify-between w-full gap-[16px]'>
        <div className='flex flex-col flex-[0.5] gap-[8px]'>
          <div className='flex text-[14px] leading-[16px] font-[500]'><p>Supplier name</p> <p className='text-[red]'>*</p></div>
          <Combobox value={selected} onChange={setSelected}>
            <div className="relative mt-1">
              <div className="relative w-full cursor-pointer overflow-hidden rounded-lg bg-[#F0F0F0] text-left">
                <Combobox.Input
                  className="w-full border-none px-[16px] py-[17px] text-sm leading-5 text-[#979797] focus:ring-0 cursor-pointer bg-[#F0F0F0] focus:outline-none"
                  displayValue={(person: any) => person.name}
                  onChange={(event) => setQuery(event.target.value)}
                />
                <Combobox.Button className="absolute inset-y-0 right-0 flex items-center pr-2">
                  <IoIosArrowDown className="h-5 w-5" />
                </Combobox.Button>
              </div>
              <Transition
                as={Fragment}
                leave="transition ease-in duration-100"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
                afterLeave={() => setQuery('')}
              >
                <Combobox.Options className="absolute mt-1 max-h-60 w-full overflow-auto rounded-[8px] bg-[#F0F0F0] py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                  {filteredPeople.length === 0 && query !== '' ? (
                    <div className="relative cursor-default select-none py-2 px-4 text-gray-700">
                      Nothing found.
                    </div>
                  ) : (
                    filteredPeople.map((person) => (
                      <Combobox.Option
                        key={person.id}
                        className={({ active }) =>
                          `relative cursor-default select-none py-2 pl-10 pr-4 ${active ? 'bg-gray text-black' : 'text-gray-900'
                          }`
                        }
                        value={person}
                      >
                        {({ selected, active }) => (
                          <>
                            <span
                              className={`block truncate ${selected ? 'font-medium' : 'font-normal'
                                }`}
                            >
                              {person.name}
                            </span>
                            {selected ? (
                              <span
                                className={`absolute inset-y-0 left-0 flex items-center pl-3 ${active ? 'text-white' : 'text-teal-600'
                                  }`}
                              >
                                {/* <CheckIcon className="h-5 w-5" aria-hidden="true" /> */}
                              </span>
                            ) : null}
                          </>
                        )}
                      </Combobox.Option>
                    ))
                  )}
                </Combobox.Options>
              </Transition>
            </div>
          </Combobox>
        </div>
        <div className='flex flex-col flex-[0.5] gap-[8px]'>
          <div className='flex text-[14px] leading-[16px] font-[500]'><p>Date</p> <p className='text-[red]'>*</p></div>
          <input type='date' className='px-[16px] py-[14px] bg-[#F0F0F0] text-[#979797] rounded-[8px]' />
        </div>
      </div>
      <div className='flex flex-col w-[49.5%] gap-[8px]'>
        <div className='flex text-[14px] leading-[16px] font-[500]'><p>Refrence</p> <p className='text-[red]'>*</p></div>
        <input type='text' className='p-[16px] bg-[#F0F0F0] text-[#979797] rounded-[8px]' placeholder="Enter refrence" />
      </div>
    </div>
  )
}

export default SupplierData